
const RemoveButton = ({onClick}) => {

    return (
        <div>
            <button className="button remove" onClick={onClick}>
                Remove Boot
            </button>
        </div>
    )

};

export default RemoveButton;