import React from "react";

const AdjustButton = (
    {onClick}
) => {

    return (
        <div>
            <button className="button adjust" onClick={onClick}>
                Adjust Boot
            </button>
        </div>
    )

};

export default AdjustButton;