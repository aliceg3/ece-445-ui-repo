import './App.css';
import AdjustButton from './components/adjustButton';
import RemoveButton from './components/removeButton';
import React from 'react';

function App() {
  const [topStrapValue, setTopStrapValue] = React.useState(0);

  const handleAdjustClick = () => {
    setTopStrapValue(topStrapValue+1);
  }

  const handleRemoveClick = () => {
    setTopStrapValue(0);
  }

  return (
    <div className="App">
      <header className="App-header">
        ECE 445 Group 10
        <div className="App-subheader">
          Remotely Adjustable Cast
        </div>
        <div className="doctor-settings">
          Doctor Settings:
          <hr class="solid" />
          <div className="modules">
            <div className="module">
              <div className="module-name">
                Top Strap
              </div>
              <div>
                <span className="module-value">45 </span><span className="module-unit">in*lbs</span>
              </div>
            </div>
            <div className="module">
              <div className="module-name">
                Bottom Strap
              </div>
              <div>
                <span className="module-value">34 </span><span className="module-unit">in*lbs</span>
              </div>
            </div>
            <div className="module">
              <div className="module-name">
                Left Air Cell
              </div>
              <div>
                <span className="module-value">12 </span><span className="module-unit">mmHg</span>
              </div>
            </div>
            <div className="module">
              <div className="module-name">
                Right Air Cell
              </div>
              <div>
                <span className="module-value">14.5 </span><span className="module-unit">mmHg</span>
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />
        <div className="boot-settings">
          Boot Settings:
          <hr class="solid" />
          <div className="modules">
            <div className="module">
              <div className="module-name">
                Top Strap
              </div>
              <div>
                <span className="module-value low">{topStrapValue} </span><span className="module-unit">in*lbs</span>
              </div>
            </div>
            <div className="module">
              <div className="module-name">
                Bottom Strap
              </div>
              <div>
                <span className="module-value good">33.8 </span><span className="module-unit">in*lbs</span>
              </div>
            </div>
            <div className="module">
              <div className="module-name">
                Left Air Cell
              </div>
              <div>
                <span className="module-value good">12.1 </span><span className="module-unit">mmHg</span>
              </div>
            </div>
            <div className="module">
              <div className="module-name">
                Right Air Cell
              </div>
              <div>
                <span className="module-value high">16 </span><span className="module-unit">mmHg</span>
              </div>
            </div>
          </div>
        </div>
        <div className="controls">
          <AdjustButton onClick={handleAdjustClick}/>
          <RemoveButton onClick={handleRemoveClick}/>
        </div>
      </header>
    </div>
  );
}

export default App;