/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com  
*********/

// Load Wi-Fi library
#include <WiFi.h>
#include <MultiStepper.h>
#include <AccelStepper.h>
#include <Stepper.h>

// Replace with your network credentials
const char* ssid = "Jack";
const char* password = "temp_pass1234";

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
String output26State = "off";
String output27State = "off";
String motorSpeedState = "off";

// Assign output variables to GPIO pins
const int output26 = 2;
//const int output27 = 27;
const int motorPin1 = 16;
const int motorPin2 = 17;
const int motorPin3 = 18;
const int motorPin4 = 19;

int motorSpeedInt;

AccelStepper myStepper(AccelStepper::FULL4WIRE, motorPin1, motorPin2, motorPin3, motorPin4);
const int steps_per_rev = 200;
float maxSetSpeed = 200;
const float speed1 = ((((maxSetSpeed/10)*(steps_per_rev/10))*10)/(6));
float setSetSpeed = 60;
const float speed2 = ((((setSetSpeed/10)*(steps_per_rev/10))*10)/(6));
float steps_per_rotation=200;
float start_max_speed = 200; // in rpm, arbitrary start max speed, for testing*****
//myStepper.setSpeed();  // ***testing, may work for constant speed 
float min_speed = 20;        // set to be the speed at which we apply the goal torque, based on chart maybe 600 but that seems too fast
float same_step_range = 10;   // min number of steps allowed before increasing the same position count, test this range*****
float same_position_count = 0;   // number of consecutive steps where the current position is within same_step_range as the previous position
float previous_pos=myStepper.currentPosition()-same_step_range;
float new_pos=200; // 200(1 rotation) is arbitrary number of steps to increase each iteration, **possibly make new target proportional to speed
float reduce_rpm = 12;  // when using set speed, reduces speed by 40 steps per second. => 40*60/200 = 2400/200 = 12rpm (rpm/0.3=num)

// Current time
unsigned long currentTime = millis();
// Previous time
unsigned long previousTime = 0; 
// Define timeout time in milliseconds (example: 2000ms = 2s)
const long timeoutTime = 2000;

void setup() {
  Serial.begin(115200);
  // Initialize the output variables as outputs
  pinMode(output26, OUTPUT);
//  pinMode(output27, OUTPUT);
  // Set outputs to LOW
  digitalWrite(output26, LOW);
//  digitalWrite(output27, LOW);

  myStepper.setMaxSpeed(start_max_speed*steps_per_rotation*60);   // based on chart maybe 1000 but that seems way too fast


  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    currentTime = millis();
    previousTime = currentTime;
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected() && currentTime - previousTime <= timeoutTime) {  // loop while the client's connected
      currentTime = millis();
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
            if (header.indexOf("GET /26/on") >= 0) {
              Serial.println("GPIO 26 on");
              output26State = "on";
              digitalWrite(output26, HIGH);
            } else if (header.indexOf("GET /26/off") >= 0) {
              Serial.println("GPIO 26 off");
              output26State = "off";
              digitalWrite(output26, LOW);
            } else if (header.indexOf("GET /motor/on") >= 0) {
              myStepper.setMaxSpeed(speed2);
              myStepper.moveTo(2000);
              
              while (myStepper.distanceToGo()>=1000) {
                motorSpeedInt = myStepper.speed();
                motorSpeedState = String(motorSpeedInt);
                Serial.println(myStepper.speed());
                myStepper.run();
                myStepper.setMaxSpeed(50);
                myStepper.runToPosition();
                Serial.println(myStepper.speed());
              } 
//                myStepper.setCurrentPosition(0);
//                myStepper.setSpeed(start_max_speed);
//                myStepper.moveTo(1);
//                myStepper.runSpeed();
//                while(myStepper.speed()>=min_speed){
//                  motorSpeedInt = myStepper.speed();
//                  motorSpeedState = String(motorSpeedInt);
//                  myStepper.runToNewPosition(myStepper.targetPosition()+new_pos);  
//                  if(myStepper.currentPosition()-previous_pos<same_step_range){
//                    same_position_count+=1;
//                  }
//                  else{
//                    same_position_count=0;
//                  }
//                  if(same_position_count>1){  // check if it has stayed in the same position for the past two turns.
//                    // setMaxSpeed(cur_speed-=40);   // use if there is no way to implement this with the constant speed function. 
//                    myStepper.setSpeed(myStepper.speed()-(reduce_rpm/0.3)); 
//                  }
//                  previous_pos=myStepper.currentPosition();
//                }
              motorSpeedState = "motor ran";
            } else if (header.indexOf("GET /motor/off") >= 0) {
              motorSpeedState = "off";
              myStepper.stop();
            }
            
            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons 
            // Feel free to change the background-color and font-size attributes to fit your preferences
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #555555;}</style></head>");
            
            // Web Page Heading
            client.println("<body><h1>ESP32 Web Server</h1>");
            
            // Display current state, and ON/OFF buttons for GPIO 26  
            client.println("<p>GPIO 26 - State " + output26State + "</p>");
            // If the output26State is off, it displays the ON button       
            if (output26State=="off") {
              client.println("<p><a href=\"/26/on\"><button class=\"button\">ON</button></a></p>");
            } else {
              client.println("<p><a href=\"/26/off\"><button class=\"button button2\">OFF</button></a></p>");
            } 
               
            // Display current state, and ON/OFF buttons for Motor  
            client.println("<p>Motor - Speed State " + motorSpeedState + "</p>");
            if (motorSpeedState=="off") {
              client.println("<p><a href=\"/motor/on\"><button class=\"button\">ON</button></a></p>");
            } else {
              client.println("<p><a href=\"/motor/off\"><button class=\"button button2\">OFF</button></a></p>");
            } 
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}