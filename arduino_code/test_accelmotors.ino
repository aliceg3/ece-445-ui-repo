#include <MultiStepper.h>
#include <AccelStepper.h>

#include <Stepper.h>


const int steps_per_rev = 200; //Set to 200 for NIMA 17 and set to 48 for 28BYJ-48
#define IN1 33
#define IN2 32
#define IN3 26
#define IN4 27

Stepper motor(steps_per_rev, IN1, IN2, IN3, IN4);

//AccelStepper myStepper(AccelStepper::FULL4WIRE, IN1, IN2, IN3, IN4);
//float maxSetSpeed = 200;
//const float speed1 = ((((maxSetSpeed/10)*(steps_per_rev/10))*10)/(6));
//float setSetSpeed = 60;
//const float speed2 = ((((setSetSpeed/10)*(steps_per_rev/10))*10)/(6));

void setup()
{
  
  Serial.begin(9600);
  motor.setSpeed(60);   // this limits the value of setSpeed(). Raise it if you like.
//  myStepper.setSpeed(50);
//  myStepper.setCurrentPosition(0);
//  myStepper.setAcceleration(10);
}

void loop() 
{
  
//  Serial.println("Rotating Clockwise...");
 // motor.setSpeed(60);
  //motor.step(steps_per_rev);
  //delay(500);

//  Serial.println("Rotating Anti-clockwise...");
  //motor.step(steps_per_rev);
  //delay(500);
//  mySpeed += 10;
//  myStepper.setSpeed(mySpeed); 
//    myStepper.setMaxSpeed(speed2);
//    myStepper.moveTo(2000);
//   
//    while (myStepper.distanceToGo() >= 1000) {
//      Serial.println(myStepper.speed());
//      
//      myStepper.run();}
//      
//      myStepper.setMaxSpeed(50);
//      myStepper.runToPosition();
//      Serial.println(myStepper.speed());

motor.step(steps_per_rev);
      
    //myStepper.run();
    // delay(1000);
}
