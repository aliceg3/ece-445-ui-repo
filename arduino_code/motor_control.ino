
const int motorPin1 = 27;
const int motorPin2 = 26;
const int motorPin3 = 15;
const int motorPin4 = 13;

AccelStepper myStepper(AccelStepper::FULL4WIRE, motorPin1, motorPin2, motorPin3, motorPin4);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600)

  float steps_per_rotation=200;
  float start_max_speed = 200; // in rpm, arbitrary start max speed, for testing*****
  myStepper.setMaxSpeed(start_max_speed);   // based on chart maybe 1000 but that seems way too fast
  myStepper.setSpeed(start_max_speed*steps_per_rotation*60);  // ***testing, may work for constant speed 
  float min_speed = 20;        // set to be the speed at which we apply the goal torque, based on chart maybe 600 but that seems too fast
  float same_step_range = 10;   // min number of steps allowed before increasing the same position count, test this range*****
  float same_position_count = 0;   // number of consecutive steps where the current position is within same_step_range as the previous position
  float previous_pos=myStepper.currentPosition()-same_step_range
  float new_pos=200 // 200(1 rotation) is arbitrary number of steps to increase each iteration, **possibly make new target proportional to speed
  float reduce_rpm = 12;  // when using set speed, reduces speed by 40 steps per second. => 40*60/200 = 2400/200 = 12rpm (rpm/0.3=num)
}

void loop() {
  // put your main code here, to run repeatedly:
  myStepper.runToPosition(1)
  while(cur_speed>=min_speed){
    myStepper.runToNewPosition(myStepper.targetPosition()+new_pos);  
    if(myStepper.currentPosition()-previous_pos<same_step_range){
      same_position_count+=1;
    }
    else{
      same_position_count=0;
    }
    if(same_position_count>1){  // check if it has stayed in the same position for the past two turns.
      // setMaxSpeed(cur_speed-=40);   // use if there is no way to implement this with the constant speed function. 
      myStepper.setSpeed(myStepper.speed()-(reduce_rpm/0.3)); 
    }
    previous_pos=myStepper.currentPosition()
  }
  

}
