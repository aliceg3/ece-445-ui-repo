/*********
 https://randomnerdtutorials.com/esp32-async-web-server-espasyncwebserver-library/
*********/

// Import required libraries
#include <Arduino.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncTCP.h>
#include <Stepper.h>
#include <AccelStepper.h>
#include <MultiStepper.h>

// Replace with your network credentials
const char* ssid = "Alice";
const char* password = "temp_pass1234";

const char* input_parameter1 = "output";
const char* input_parameter2 = "state";

#define PRESSUREPIN1 34
#define PRESSUREPIN2 35

#define motor1IN1 33
#define motor1IN2 32
#define motor1IN3 26
#define motor1IN4 27

#define motor2IN1 13
#define motor2IN2 14
#define motor2IN3 15
#define motor2IN4 4

#define PRESSURE1GOAL 22
#define PRESSURE2GOAL 19

#define MOTORTORQUE .6

float MOTORTENSION = MOTORTORQUE / .1778;

Stepper motor1(200, motor1IN1, motor1IN2, motor1IN3, motor1IN4);
Stepper motor2(200, motor2IN1, motor2IN2, motor2IN3, motor2IN4);

int pressure1 = 0.0;
int pressure2 = 0.0;
float motor1Speed = 0.0;
float motor2Speed = 0.0;
String pressure1status = "NOT YET...";
String pressure2status = "NOT YET...";
String motor1status = "Initialized";
String motor2status = "Initialized";

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;    // will store last time DHT was updated

// Updates DHT readings every 10 seconds
const long interval = 1;  

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <style>
    html {
     font-family: Arial;
     display: inline-block;
     margin: 0px auto;
     text-align: center;
    }
    h2 { font-size: 1.5rem; }
    h4 {display: inline; padding:5px};
    h5 {display: block};
    p { font-size: 1.5rem; }
    .units { font-size: 1.0rem; }
    .sensorlabel{
      font-size: 1.0rem;
      vertical-align:center;
    }
    .statuslabel { font-size: 1.3rem; 
      padding-bottom: 15px;}
    button2 {background-color: #555555;}
    body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
    .switch {position: relative; display: inline-block; width: 120px; height: 68px} 
    .switch input {display: none}
    .sliderON {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 6px}
    .sliderON:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 3px}
    input:checked+.sliderON {background-color: #20A110}
    input:checked+.sliderON:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}
    .sliderOFF {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 6px}
    .sliderOFF:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 3px}
    input:checked+.sliderOFF {background-color: #b30000}
    input:checked+.sliderOFF:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}
  </style>
</head>
<body>
  <h2>Remotely Adjustable Boot</h2>
  <p>
    <i class="fas fa-thermometer-half" style="color:#4a32a8;"></i> 
    <b><span class="sensorlabel">Pressure LEFT - Prescribed Pressure: %PRESSURE1GOAL% mmHg</span></b>
    <br />
    <span id="pressure1" class="statuslabel">%PRESSURE1%</span>
    <sup class="units">mmHg</sup>
  </p>
  <p>
    <span class="sensorlabel">Status:</span>
    <span id="pressure1status" class="statuslabel">%PRESSURE1STATUS%</span>
  </p>
  <p>
    <i class="fas fa-thermometer-half" style="color:#4a32a8;"></i> 
    <b><span class="sensorlabel">Pressure RIGHT - Prescribed Pressure: %PRESSURE2GOAL% mmHg</span></b>
    <br />
    <span id="pressure2" class="statuslabel">%PRESSURE2%</span>
    <sup class="units">mmHg</sup>
  </p>
  <p>
    <span class="sensorlabel">Status:</span>
    <span id="pressure2status" class="statuslabel">%PRESSURE2STATUS%</span>
  </p>
  <p>
    <i class="fas fa-wrench" style="color:#4a32a8;"></i> 
    <b><span class="sensorlabel">Ankle Motor - Prescribed Tension: %MOTORTENSION% N*m</span></b>
    <br />
    <span class="sensorlabel">Status:</span>
    <span id="motor1status" class="statuslabel">%motor1STATUS%</span>
  </p>
  <p>
    <i class="fas fa-wrench" style="color:#4a32a8;"></i> 
    <b><span class="sensorlabel">Foot Motor - Prescribed Tension: %MOTORTENSION% N*m</span></b>
    <br />
    <span class="sensorlabel">Status:</span>
    <span id="motor2status" class="statuslabel">%motor2STATUS%</span>
  </p>
  <p>
    <h4>Tighten Boot Straps</h4>
    <h4>Loosen Boot Straps</h4>
  </p>
  %BUTTONPLACEHOLDER%
  <script>function toggleCheckbox(element) {
    var xhr = new XMLHttpRequest();
    if(element.checked){ xhr.open("GET", "/update?output="+element.id+"&state=1", true); }
    else { xhr.open("GET", "/update?output="+element.id+"&state=0", true); }
    xhr.send();
  }
  </script>
</body>
<script>
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("pressure1").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/pressure1", true);
  xhttp.send();
}, 1000 ) ;

setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("pressure1status").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/pressure1status", true);
  xhttp.send();
}, 1000 ) ;

setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("pressure2").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/pressure2", true);
  xhttp.send();
}, 1000 ) ;

setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("pressure2status").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/pressure2status", true);
  xhttp.send();
}, 1000 ) ;

setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("motor1status").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/motor1status", true);
  xhttp.send();
}, 1000 ) ;

setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("motor2status").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/motor2status", true);
  xhttp.send();
}, 1000 ) ;
</script>
</html>)rawliteral";

String outputState(int output){
  if(digitalRead(output)){
    return "checked";
  }
  else {
    return "";
  }
}

// Replaces placeholder with DHT values
String processor(const String& var){
  //Serial.println(var);
  if(var == "PRESSURE1"){
    return String(pressure1);
  }
  else if (var == "PRESSURE1STATUS"){
    return String(pressure1status);
  }
  else if(var == "PRESSURE1GOAL"){
    return String(PRESSURE1GOAL);
  }
  else if(var == "PRESSURE2"){
    return String(pressure2);
  }
  else if (var == "PRESSURE2STATUS"){
    return String(pressure2status);
  }
  else if(var == "PRESSURE2GOAL"){
    return String(PRESSURE2GOAL);
  }
  else if(var == "motor1STATUS"){
    return String(motor1status);
  }
  else if(var == "motor2STATUS"){
    return String(motor2status);
  }
  else if (var == "MOTORTENSION"){
    return String(MOTORTENSION);
  }
  else if(var == "BUTTONPLACEHOLDER"){
    String buttons = "";
    buttons += "<h4></h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"1\" " + outputState(2) + "><span class=\"sliderON\"></span></label>";
    buttons += "<h4></h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"2\" " + outputState(2) + "><span class=\"sliderOFF\"></span></label>";
    return buttons;
  }
  return String();
}

void setup(){
  
  // Serial port for debugging purposes
  Serial.begin(115200);
 
  
  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  Serial.println("Connecting to WiFi");
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println(".");
  }

  // Print ESP8266 Local IP Address
  Serial.println(WiFi.localIP());

    pinMode(2, OUTPUT);
  digitalWrite(2, LOW);

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });
  server.on("/pressure1", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(pressure1).c_str());
  });
  server.on("/pressure1status", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(pressure1status).c_str());
  });
  server.on("/pressure2", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(pressure2).c_str());
  });
  server.on("/pressure2status", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(pressure2status).c_str());
  });
  server.on("/motor1status", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(motor1status).c_str());
  });
  server.on("/motor2status", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(motor2status).c_str());
  });

  server.on("/update", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String inputMessage1;
    String inputMessage2;
    // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
    if (request->hasParam(input_parameter1) && request->hasParam(input_parameter2)) {
      inputMessage1 = request->getParam(input_parameter1)->value();
      inputMessage2 = request->getParam(input_parameter2)->value();
      digitalWrite(inputMessage1.toInt(), inputMessage2.toInt());
    }
    else {
      inputMessage1 = "No message sent";
      inputMessage2 = "No message sent";
    }
    Serial.print("GPIO: ");
    Serial.print(inputMessage1);
    Serial.print(" - Set to: ");
    Serial.println(inputMessage2);

    if (inputMessage1.toInt() == 1){
      if (inputMessage2.toInt() == 1){
        motor1status = "Tightening..."; // doesn't work idk why
        motor2status = "Waiting...";
      }
      if (inputMessage2.toInt() == 0){
        motor1status = "Off"; 
        motor2status = "Off";
      } 
    }

    if (inputMessage1.toInt() == 2) {
      if (inputMessage2.toInt() == 1){
        motor1status = "Loosening..."; // doesn't work idk why
        motor2status = "Waiting...";
      }
      if (inputMessage2.toInt() == 0){
        motor1status = "Off"; 
        motor2status = "Off";
      } 
    }

    
    request->send(200, "text/plain", "OK");
  });

  // Start server
  server.begin();

}
 
void loop(){
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    
    previousMillis = currentMillis;
    
    int intervals1[7] = {0,0,0,0,0,0,0};
    int intervalValues1[7] = {0,0,0,0,0,0,0};
    int intervals2[7] = {0,0,0,0,0,0,0};
    int intervalValues2[7] = {0,0,0,0,0,0,0};

    if (motor1status == "Tightening..."){
      motor1.setSpeed(60);
      motor1.step(600);    
      
      digitalWrite(motor1IN1, LOW);
      digitalWrite(motor1IN2, LOW);
      digitalWrite(motor1IN3, LOW);
      digitalWrite(motor1IN4, LOW);
      motor1status = "Done";

      motor2status = "Tightening...";

      motor2.setSpeed(60);
      motor2.step(-500);

      digitalWrite(motor2IN1, LOW);
      digitalWrite(motor2IN2, LOW);
      digitalWrite(motor2IN3, LOW);
      digitalWrite(motor2IN4, LOW);
      motor2status = "Done";
    }

    if (String(motor1status) == "Loosening..."){
      motor1.setSpeed(60);
      motor1.step(-400);
      
      digitalWrite(motor1IN1, LOW);
      digitalWrite(motor1IN2, LOW);
      digitalWrite(motor1IN3, LOW);
      digitalWrite(motor1IN4, LOW);
      motor1status = "Done";

      motor2status = "Loosening...";

      motor2.setSpeed(60);
      motor2.step(300);

      digitalWrite(motor2IN1, LOW);
      digitalWrite(motor2IN2, LOW);
      digitalWrite(motor2IN3, LOW);
      digitalWrite(motor2IN4, LOW);
      motor2status = "Done";
    }

     for(int counter = 0; counter < 10; counter ++) {
      int a1 = analogRead(PRESSUREPIN1);
      int a2 = analogRead(PRESSUREPIN2);
      int aModulus1 = int (a1 / (600));
      int aModulus2 = int (a2 / (600));
      intervals1[aModulus1] += 1;
      intervalValues1[aModulus1] += int(a1 % 600);

      intervals2[aModulus2] += 1;
      intervalValues2[aModulus2] += int(a2 % 600);

      delay(10);
     }
          
     int maxIntervalSize1 = intervals1[0];
     int maxIntervalIndex1 = 0;
     for (int index = 1; index < 7; index ++) {
      if (intervals1[index] > maxIntervalSize1) {
        maxIntervalIndex1 =  index;
        maxIntervalSize1 = intervals1[index];
      }
     }
     int maxIntervalSize2 = intervals2[0];
     int maxIntervalIndex2 = 0;
     for (int index = 1; index < 7; index ++) {
      if (intervals2[index] > maxIntervalSize2) {
        maxIntervalIndex2 =  index;
        maxIntervalSize2 = intervals2[index];
      }
     }
     pressure1 = (intervalValues1[maxIntervalIndex1]/maxIntervalSize1)+ maxIntervalIndex1*600;
     pressure1 = map(pressure1, 0, 4095, 0, 51.671); // max mmHg reading our FSR can take 406
     pressure2 = (intervalValues2[maxIntervalIndex2]/maxIntervalSize2)+ maxIntervalIndex2*600;
     pressure2 = map(pressure2, 0, 4095, 0, 51.671); // max mmHg reading our FSR can take 406

    if (((float)abs(pressure1 - PRESSURE1GOAL)/(float)PRESSURE1GOAL) <= 0.15){
      pressure1status = "LEFT GOAL REACHED";
    }
    else {
      pressure1status = "Not yet...";
    }

    if (((float)abs(pressure2 - PRESSURE2GOAL)/(float)PRESSURE2GOAL) <= 0.15){
      pressure2status = "RIGHT GOAL REACHED";
    }
    else {
      pressure2status = "Not yet...";
    }
    
  }
}