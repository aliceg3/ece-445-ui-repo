/*
 * ESP32 AJAX Demo
 * Updates and Gets data from webpage without page refresh
 * https://circuits4you.com
 */
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>

#include "index.h"  //Web page header file

WebServer server(80);

//Enter your SSID and PASSWORD
const char* ssid = "Saloni's iphone (2)";
const char* password = "saloni29";

//===============================================================
// This routine is executed when you open its IP in browser
//===============================================================
void handleRoot() {
 String s = MAIN_page; //Read HTML contents
 server.send(200, "text/html", s); //Send web page
}
 
void handleADC() {
 int intervals[7] = {0,0,0,0,0,0,0};
 int intervalValues[7] = {0,0,0,0,0,0,0};
 
 Serial.println("**********");

 for(int counter = 0; counter < 10; counter ++) {
  int a = analogRead(34);
  int aModulus = int (a / (600));
  intervals[aModulus] += 1;
  intervalValues[aModulus] += int(a % 600);

  Serial.println("Reading #" + String(counter) + ": " + String(a) + ", " + String(aModulus));

  delay(10);
 }

 Serial.println("Intervals array: [");
 for (int counter = 0; counter < 7; counter++) {
  Serial.print(intervals[counter]);
  Serial.print(",");
 }
 Serial.print("]");

 Serial.println("Interval Values array: ");
 for (int counter = 0; counter < 7; counter++) {
  Serial.print(intervalValues[counter]);
  Serial.print(",");
 }
 Serial.print("]");

 Serial.println("**********");
 
 int maxIntervalSize = intervals[0];
 int maxIntervalIndex = 0;
 for (int index = 1; index < 7; index ++) {
  if (intervals[index] > maxIntervalSize) {
    maxIntervalIndex =  index;
    maxIntervalSize = intervals[index];
  }
 }
 Serial.println("maxIntervalIndex: " + maxIntervalIndex);
 int adcValueInt = (intervalValues[maxIntervalIndex]/maxIntervalSize)+ maxIntervalIndex*600;
 String adcValue = String(adcValueInt);
 Serial.println("adcValue: " + adcValue);
 Serial.println("******************************");
 
 server.send(200, "text/plane", adcValue); //Send ADC value only to client ajax request
}

//===============================================================
// Setup
//===============================================================

void setup(void){
  Serial.begin(115200);
  Serial.println();
  Serial.println("Booting Sketch...");

/*
//ESP32 As access point
  WiFi.mode(WIFI_AP); //Access Point mode
  WiFi.softAP(ssid, password);
*/
//ESP32 connects to your wifi -----------------------------------
  WiFi.mode(WIFI_STA); //Connectto your wifi
  WiFi.begin(ssid, password);

  Serial.println("Connecting to ");
  Serial.print(ssid);

  //Wait for WiFi to connect
  while(WiFi.waitForConnectResult() != WL_CONNECTED){      
      Serial.print(".");
    }
    
  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
//----------------------------------------------------------------
 
  server.on("/", handleRoot);      //This is display page
  server.on("/readADC", handleADC);//To get update of ADC Value only
 
  server.begin();                  //Start server
  Serial.println("HTTP server started");
}

//===============================================================
// This routine is executed when you open its IP in browser
//===============================================================
void loop(void){
  server.handleClient();
  delay(1);
}